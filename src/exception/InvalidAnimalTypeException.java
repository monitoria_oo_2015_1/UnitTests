package exception;

public class InvalidAnimalTypeException extends Exception {

	private static final long serialVersionUID = 2281363219611509283L;

	public InvalidAnimalTypeException() {
		super();
	}

	public InvalidAnimalTypeException(String message) {
		super(message);
	}
}
